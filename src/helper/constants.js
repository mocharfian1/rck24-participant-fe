export const STATUS = {
    PENDING: { text: 'Belum Verifikasi', icon: 'mdi-information', color: 'orange' },
    VERIFIED: { text: 'Terverifikasi', icon: 'mdi-check-circle', color: 'green' },
    REJECT: { text: 'Ditolak', icon: 'mdi-close-circle', color: 'red' },
}

export const STATUS_HISTORY = {
    PENDING: { text: 'Menunggu', icon: 'mdi-information', color: 'orange' },
    APPROVED: { text: 'Diterima', icon: 'mdi-check-circle', color: 'green' },
    REJECT: { text: 'Ditolak', icon: 'mdi-close-circle', color: 'red' },
}
