const isValidToken = function (token, hash){
    try {
        return token.toString() === hash.split('#')[1].toString()
    }catch (e){
        return false
    }

}

module.exports = {
    isValid : isValidToken
}
