import axios from 'axios'

export default {
    sendImage(url, payload){
        console.log('==> SERVICE', payload)
        return axios.post(`${url}/api/v1/camera`, payload, {
            headers: {
                'Content-Type': 'multipart/form-data',
                'authorization': localStorage.getItem('entry-token')
            }
        })
    }
}
