import axios from 'axios'

export default {
    submitLogin(url, payload){
        console.log('==> SERVICE', url)
        return axios.post(`${url}/api/v1/user/login`,payload)
    },
    checkSession(url, payload){
        console.log('===> REQUEST SESSION ', url)
        return axios.post(`${url}/api/v1/user/login/session`,{},{
            headers: {
                authorization: payload.token
            }
        })
    }
}
