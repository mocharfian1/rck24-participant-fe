import axios from 'axios'

export default {
    getProvinsi() {
        return axios.get('https://mocharfian1.github.io/api-wilayah-indonesia/api/provinces.json')
    },
    getWilayah(type, id_from) {
        console.log(id_from, type)
        const endpoint_all = {
            provinsi: 'https://mocharfian1.github.io/api-wilayah-indonesia/api/provinces.json'
        }
        const endpoint_detail = {
            kabkota: (id_from) => {
                return `https://mocharfian1.github.io/api-wilayah-indonesia/api/regencies/${id_from}.json`
            },
            kecamatan: (id_from) => {
                return `https://mocharfian1.github.io/api-wilayah-indonesia/api/districts/${id_from}.json`
            },
            kelurahan: (id_from) => {
                return `https://mocharfian1.github.io/api-wilayah-indonesia/api/villages/${id_from}.json`
            }
        }
        //
        const endpoint = id_from != null ? endpoint_detail[type](id_from):endpoint_all[type]
        console.log('===> ', endpoint)
        return axios.get(endpoint)
    },
    getKoordinator(url){
        url = `${url}/api/v1/user/register/koordinator`
        return axios.get(url)
    },
    submitRegister(url, payload){
        console.log('==> SERVICE', url)
        return axios.post(`${url}/api/v1/user/register/submit`,payload)
    }
}
