import axios from 'axios'

export default {
    getOneByCodeAndNik(url, code){
        return axios.get(url + '/api/v1/history',{
            headers: {
                'Authorization': localStorage.getItem('entry-token')
            }, params: {
                code
            }
        })
    },
    submitLogin(url, payload){
        console.log('==> SERVICE', url)
        return axios.post(url,payload)
    },
    getAllList(url){
        console.log('===> REQUEST SESSION ', url)
        return axios.get(url + '/api/v1/history',{
            headers: {
                'Authorization': localStorage.getItem('entry-token')
            }
        })
    }
}
