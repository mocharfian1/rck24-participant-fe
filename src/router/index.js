import Vue from 'vue'
import VueRouter from 'vue-router'
import LoginPage from '../components/LoginPage'
import HomePage from '../components/HomePage'
import OpenCamera from "@/components/sub_pages/OpenCamera";
import SuccessPage from "@/components/SuccessPage";
import HistoryPage from "@/components/HistoryPage";
import HistoryDetail from "@/components/history/HistoryDetail";
import RegisterPage from "@/components/RegisterPage";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: HomePage
    },
    {
        path: '/login',
        name: 'Login',
        component: LoginPage
    },
    {
        path: '/register',
        name: 'Register',
        component: RegisterPage
    },
    {
        path: '/history',
        name: 'History Page',
        component: HistoryPage
    },
    {
        path: '/history/detail',
        name: 'History Detail',
        component: HistoryDetail
    },
    {
        path: '/home/camera',
        name: 'Open Camera',
        component: OpenCamera
    },
    {
        path: '/home/camera/success',
        name: 'Success Send Image',
        component: SuccessPage
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
