import Vue from 'vue'
import Vuex from 'vuex'
import * as login from './modules/login'
import * as register from './modules/register'
import * as camera from './modules/camera'
import * as history from './modules/history'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {},
    mutations: {},
    actions: {},
    modules: {
        login,
        register,
        camera,
        history
    }
})
