import HistoryService from "../../services/HistoryService";

export const namespaced = true

export const state = {
    data: {
        item: [],
        item_detail: {}
    },
    loading: {
        submit: false,
        get_data: false,
        get_detail: false
    },
    notification: {
        show: false,
        color: null,
        text: null
    },
}

export const mutations = {
    LOADING_SUBMIT(state, loading){
        state.loading.submit = loading
    },
    LOADING_GET_DATA(state, loading){
        state.loading.get_data = loading
    },
    LOADING_GET_DETAIL(state, loading){
        state.loading.get_detail = loading
    },
    SHOW_NOTIFICATION(state, data){
        state.notification = data
    },
    SET_DATA(state, data){
        state.data.item = data
    },
    SET_DETAIL(state, data){
        state.data.item_detail = data
    }
}

export const actions = {
    getAllList({ commit }, params){
        commit('LOADING_GET_DATA', true)
        const { url } = params
        return HistoryService.getAllList(url).then((res)=>{
            commit('LOADING_GET_DATA', false)
            if(res.data.success){
                commit('SET_DATA', res.data.data)
                return true
            }
            commit('SHOW_NOTIFICATION', { show: true, color: 'red', text: res.data.messageDetail })
            return false
        }).catch((e)=>{
            console.log(e)
            commit('LOADING_GET_DATA', false)
            commit('SHOW_NOTIFICATION', { show: true, color: 'red', text: e.response.data.message })
            return false
        })
    },
    getOneByCodeAndNik({ commit }, params){
        commit('LOADING_GET_DETAIL', true)
        const { url, code } = params
        return HistoryService.getOneByCodeAndNik(url, code).then((res)=>{
            commit('LOADING_GET_DETAIL', false)
            if(res.data.success){
                commit('SET_DETAIL', res.data.data)
                return true
            }
            commit('SHOW_NOTIFICATION', { show: true, color: 'red', text: res.data.messageDetail })
            return false
        }).catch((err)=>{
            console.log(err.response)
            commit('LOADING_GET_DETAIL', false)
            commit('SHOW_NOTIFICATION', { show: true, color: 'red', text: err.response.data.message })
            return false
        })
    },
}
