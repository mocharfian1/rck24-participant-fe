import RegisterService from '../../services/RegisterService'
export const namespaced = true

export const state = {
    user: {},
    wilayah: {
        provinsi: null,
        kabkota: null,
        kecamatan: null,
        kelurahan: null
    },
    loading: {
        provinsi: false,
        kabkota: false,
        kecamatan: false,
        kelurahan: false,
        submit: false,
        koordinator: false
    },
    notification: {
        show: false,
        color: 'red',
        text: null
    },
}

export const mutations = {
    SET_DATA_WILAYAH(state, res){
        const { type, data } = res
        state.wilayah[type] = data
    },
    SET_DATA_KOORDINATOR(state, data){
        state.wilayah.koordinator = data.data
    },
    CLEAR_DATA_WILAYAH(state, arrField = []){
        arrField.forEach((value) => {
            state.wilayah[value] = null
        })
    },
    LOADING_DATA_WILAYAH(state, { type, loading }){
        state.loading[type] = loading
    },
    LOADING_DATA_KOORDINATOR(state, loading){
        state.loading.koordinator = loading
    },
    LOADING_SUBMIT(state, loading){
        state.loading.submit = loading
    },
    SHOW_NOTIFICATION(state, data){
        state.notification = data
    }
}

export const actions = {
    getWilayah({ commit, dispatch }, params) {
        console.log(commit, dispatch)
        const { type, id_from } = params
        commit('LOADING_DATA_WILAYAH', { type, loading: true })
        switch (type){
            case 'kabkota':
                commit('CLEAR_DATA_WILAYAH', ['kecamatan', 'kelurahan'])
                break;
            case 'kecamatan':
                commit('CLEAR_DATA_WILAYAH', ['kelurahan'])
                break;
        }
        return RegisterService.getWilayah(type, id_from).then((res) => {
            commit('LOADING_DATA_WILAYAH', { type, loading: false })
            commit('SET_DATA_WILAYAH', { type, data: res.data })
        }).catch(()=>{
            commit('LOADING_DATA_WILAYAH', { type, loading: false })
        })
    },
    getKoordinator({ commit }, params){
        const { url } = params
        commit('LOADING_DATA_KOORDINATOR', true)
        return RegisterService.getKoordinator(url).then((res) => {
            commit('LOADING_DATA_KOORDINATOR', false)
            commit('SET_DATA_KOORDINATOR', res.data)
        }).catch(()=>{
            commit('LOADING_DATA_KOORDINATOR', false)
        })
    },
    submitRegister({ commit }, params){
        commit('LOADING_SUBMIT', true)
        const { url, payload } = params
        return RegisterService.submitRegister(url, payload).then((res)=>{
            commit('LOADING_SUBMIT', false)
            if(res.data.success){
                commit('SHOW_NOTIFICATION', { show: true, color: 'success', text: res.data.messageDetail })
                return { ...res.data }
            }
            commit('SHOW_NOTIFICATION', { show: true, color: 'red', text: res.data.messageDetail })
            return { success: false }
        }).catch((e)=>{
            commit('LOADING_SUBMIT', false)
            commit('SHOW_NOTIFICATION', { show: true, color: 'red', text: e.response.data.messageDetail })
            return { success: false }
        })
    }
}
