import CameraService from "../../services/CameraService";

export const namespaced = true

export const state = {
    data: {
        user_information: {}
    },
    loading: {
        submit: false,
        check_session: false,
    },
    notification: {
        show: false,
        color: null,
        text: null
    },
}

export const mutations = {
    LOADING_SUBMIT(state, loading){
        state.loading.submit = loading
    },
    LOADING_CHECK_SESSION(state, loading){
        state.loading.check_session = loading
    },
    SHOW_NOTIFICATION(state, data){
        state.notification = data
    },
    SET_DATA(state, data){
        state.data.user_information = data
    }
}

export const actions = {
    sendImage({ commit }, params){
        console.log(params)
        commit('LOADING_SUBMIT', true)
        const { url, payload } = params
        return CameraService.sendImage(url, payload).then((res)=>{
            commit('LOADING_SUBMIT', false)
            if(res.data.success){
                return { success: res.data.success, ...res.data.data }
            }
            commit('SHOW_NOTIFICATION', { show: true, color: 'red', text: res.data.messageDetail })
            return { success: false }
        }).catch((e)=>{
            commit('LOADING_SUBMIT', false)
            commit('SHOW_NOTIFICATION', { show: true, color: 'red', text: e.response.data.message })
            return { success: false }
        })
    }
}
