import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import store from './store'
import router from './router'
// import axios from "axios";

Vue.config.productionTip = false
const tokenPath = new Date().getTime()

// axios.interceptors.response.use((res) => {
//   // console.log('===> ', res)
//   return res
// }, (err) => {
//   if(err.response.status === 403){
//     window.location.replace('/login')
//     localStorage.removeItem('entry-token')
//   }
//   return Promise.reject(err)
// })

Vue.mixin({
  data: function() {
    return {
      get tokenGenerate() {
        return tokenPath;
      },
      get webApi() {
        return 'https://rck24-api-participant-v1.mobileapi.masuk.id'
      }
    }
  }
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
  watch: {
    '$route': function(to, from) {
      console.log('===> TO ', to)
      console.log('===> FROM ', from)
    }
  }
}).$mount('#app')
